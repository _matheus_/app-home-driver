import 'package:app_driver/blocs/home_bloc.dart';
import 'package:app_driver/data/models/load_model.dart';
import 'package:app_driver/data/models/pendecy_model.dart';
import 'package:app_driver/data/models/user_model.dart';
import 'package:app_driver/data/repositories/load_repository_interface.dart';
import 'package:app_driver/data/repositories/pendency_repository_interdace.dart';
import 'package:app_driver/data/repositories/user_repository_interface.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class MockUserRepository extends Mock implements UserRepositoryInterface {}

class MockLoadRepository extends Mock implements LoadRepositoryInterface {}

class MockPendencyRepository extends Mock
    implements PendencyRepositoryInterface {}

void main() {
  final userRepository = MockUserRepository();
  final loadRepository = MockLoadRepository();
  final pendencyRepository = MockPendencyRepository();

  when(() => userRepository.getUser()).thenAnswer((_) async =>
      UserModel(id: '1', firstName: 'firstName', lastName: 'lastName'));

  when(() => loadRepository.getLoads(any()))
      .thenAnswer((_) async => LoadModel(items: [], total: 0));

  when(() => pendencyRepository.getPendencies(any()))
      .thenAnswer((_) async => PendencyModel(boxes: 0, orders: []));

  blocTest<HomeCubit, HomeState>(
    'deve emitir os estados [LoadingState, HomeLoaded]',
    build: () => HomeCubit(
        userRepository: userRepository,
        loadRepository: loadRepository,
        pendencyRepository: pendencyRepository),
    act: (bloc) => bloc.loadHome(),
    expect: () => [isA<LoadingState>(), isA<HomeLoaded>()],
  );
}
