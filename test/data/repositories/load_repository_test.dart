import 'dart:convert';

import 'package:app_driver/data/models/load_model.dart';
import 'package:app_driver/data/repositories/load_repository.dart';
import 'package:app_driver/data/services/http_service.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../mocks/json_mocks.dart';

class MockHttpService extends Mock implements HttpService {}

void main() {
  late MockHttpService httpServiceMock;

  setUpAll(() {
    httpServiceMock = MockHttpService();
  });

  tearDown(() {
    reset(httpServiceMock);
  });

  test('deve chamar a funcao getLoads e retornar um List<LoadModel>', () async {
    when(() => httpServiceMock.getRequest(any(),
            params: any(named: 'params', that: isA<Map>())))
        .thenAnswer((invocation) async =>
            Response(statusCode: 200, content: jsonDecode(loadJson)));

    final repository = LoadRepository(httpService: httpServiceMock);
    final load = await repository.getLoads('1');

    expect(load, isA<LoadModel>());
  });

  test('deve chamar a funca ogetLoads e lançar uma Exception', () {
    when(() => httpServiceMock.getRequest(any(),
            params: any(named: 'params', that: isA<Map>())))
        .thenAnswer((invocation) async => Response(statusCode: 400));

    final repository = LoadRepository(httpService: httpServiceMock);

    expect(() => repository.getLoads('1'), throwsException);
  });
}
