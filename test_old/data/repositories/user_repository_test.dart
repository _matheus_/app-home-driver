import 'package:app_driver/data/models/user_model.dart';
import 'package:app_driver/data/repositories/user_repository.dart';
import 'package:app_driver/data/services/http_service.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  final repository = UserRepository(httpService: HttpService());

  test('should get user data and returns a userModel instance', () async {
    var user = await repository.getUser();

    expect(user, isA<UserModel>());
  });
}
