import 'dart:convert';

import 'package:app_driver/data/models/load_model.dart';
import 'package:app_driver/data/repositories/load_repository.dart';
import 'package:app_driver/data/repositories/load_repository_interface.dart';
import 'package:app_driver/data/services/http_service.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:mocktail/mocktail.dart';

import '../../mocks/json_mocks.dart';

class MockClient extends Mock implements http.Client {}

// class MockHttpService extends Mock implements HttpService {}

class FakeUri extends Fake implements Uri {}

void main() {
  setUpAll(() {
    registerFallbackValue(FakeUri());
  });

  test('should get driver loads and returns a LoadModel instance', () async {
    final mockClient = MockClient();
    final loadRepository = LoadRepository(httpService: HttpService(mockClient));
    when(() => mockClient.get(any()))
        .thenAnswer((_) async => http.Response(loadJson, 200));

    final loadModel = await loadRepository.getLoads('500');
    expect(loadModel, isA<LoadModel>());
  });

  test('should get driver load and throws an exception', () async {
    final mockClient = MockClient();

    when(() => mockClient.get(any()))
        .thenAnswer((_) async => http.Response(loadJson, 500));

    final loadRepository = LoadRepository(httpService: HttpService(mockClient));

    //final loadModel = await loadRepository.getLoads('500');
    expect(() => loadRepository.getLoads('500'), throwsException);
  });
}
