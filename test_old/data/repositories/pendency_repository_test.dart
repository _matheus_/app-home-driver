import 'package:app_driver/data/models/pendecy_model.dart';
import 'package:app_driver/data/repositories/pendency_repository.dart';
import 'package:app_driver/data/repositories/user_repository_interface.dart';
import 'package:app_driver/data/services/http_service.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class UserRepositotyMock extends Mock implements UserRepositoryInterface {}

void main() {
  final userRepositoryMock = UserRepositotyMock();
  const token =
      'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL3d3dy5tZXJ5dG8uY29tLmJyLyIsImlhdCI6MTYzNTI1MTQ1NCwibmJmIjoxNjM1MjUxNDU0LCJleHAiOjE2MzU4NTYyNTQsImRhdGEiOnsidXNlciI6eyJpZCI6IjUwMCIsInNlc3Npb24iOiJmODk2MTg4ZWE5ZGE0YTczYWE0YmIwNzA0NmM2OWM0NyJ9fX0.gUEOZnSK_SovDnWFGJcAanI9L4qc3sQc1z9g65YQDZI';

  when(() => userRepositoryMock.getToken())
      .thenAnswer((_) => Future.value(token));

  final pendencyRepository = PendencyRepository(
      httpService: HttpService(), userRepository: userRepositoryMock);
  test('should get driver pendency and returns PendecyModel instance',
      () async {
    final pendency = await pendencyRepository.getPendencies('500');

    expect(pendency, isA<PendencyModel>());
  });
}
