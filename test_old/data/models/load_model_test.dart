import 'package:app_driver/data/models/load_model.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('deve existir carregamento', () {
    final load = LoadModel(items: [
      LoadItem(id: 1, accepted: true, dispatchedBoxes: 1),
    ], total: 1);

    expect(load.hasLoad, equals(true));
  });
}
