import 'package:app_driver/data/models/pendecy_model.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('should return true when call hasPendency', () {
    final model = PendencyModel(boxes: 9, orders: [1, 2, 4]);

    expect(model.hasPendency, equals(true));
  });

  test('should return false whe call hasPendency', () {
    final model = PendencyModel(boxes: 0, orders: []);

    expect(model.hasPendency, equals(false));
  });
}
