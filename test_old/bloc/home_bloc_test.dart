import 'dart:convert';

import 'package:app_driver/blocs/home_bloc.dart';
import 'package:app_driver/data/models/pendecy_model.dart';
import 'package:app_driver/data/models/user_model.dart';
import 'package:app_driver/data/repositories/load_repository_interface.dart';
import 'package:app_driver/data/repositories/pendency_repository_interdace.dart';
import 'package:app_driver/data/repositories/user_repository_interface.dart';
// import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../mocks/json_mocks.dart';

class UserRepositoryMock extends Mock implements UserRepositoryInterface {}

class LoadRepositoryMock extends Mock implements LoadRepositoryInterface {}

class PendecyRepositoryMock extends Mock
    implements PendencyRepositoryInterface {}

void main() {
  late HomeCubit homeCubit;
  final userRepo = UserRepositoryMock();
  final loadRepo = LoadRepositoryMock();
  final pendencyRepo = PendecyRepositoryMock();

  when(() => userRepo.getUser()).thenAnswer(
      (_) => Future.value(UserModel.fromJson(jsonDecode(userJson))));

  when(() => loadRepo.getLoads(any()))
      .thenAnswer((_) => Future.value(jsonDecode(loadJson)));

  when(() => pendencyRepo.getPendencies(any())).thenAnswer(
      (_) => Future.value(PendencyModel.fromJson(jsonDecode(pendencyJson))));

  setUp(() {
    homeCubit = HomeCubit(
        userRepository: userRepo,
        loadRepository: loadRepo,
        pendencyRepository: pendencyRepo);
  });

  // blocTest<HomeCubit, HomeState>(
  //   'emits [LoadingState, HomeLoaded] states for successful home load',
  //   build: () => homeCubit,
  //   act: (HomeCubit cubit) => cubit.loadHome(),
  //   expect: () => [
  //     isA<LoadingState>(),
  //     isA<HomeLoaded>(),
  //   ],
  // );

  // blocTest<HomeCubit, HomeState>('emits FailedState',
  //     build: () => homeCubit,
  //     act: (cubit) => cubit.emitFail(),
  //     expect: () => [isA<FailedLoadingHome>(), isA<InitialState>()]);
}
