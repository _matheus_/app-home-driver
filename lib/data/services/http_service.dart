import 'dart:convert';

import 'package:http/http.dart' as http;

class Response {
  int statusCode;
  Map<String, dynamic>? content;
  Exception? exception;

  Response({
    required this.statusCode,
    this.content,
    this.exception,
  });

  bool get success => statusCode == 200;
}

class HttpService {
  // static final HttpService _instance = HttpService._internal();
  final http.Client client;

  HttpService(this.client);

  // HttpService._internal();

  Future<Response> getRequest(String uri,
      {Map<String, dynamic>? params, Map<String, String>? headers}) async {
    if (params != null) {
      uri += _buildQueryString(params);
    }

    final httpResponse = await client.get(Uri.parse(uri), headers: headers);

    return _parseHttpReponse(httpResponse);
  }

  String _buildQueryString(Map<String, dynamic> params) {
    var output = '?';
    params.forEach((key, value) {
      output += '$key=$value&';
    });

    return output;
  }

  Response _parseHttpReponse(http.Response httpResponse) {
    Response response;
    try {
      response = Response(
          statusCode: httpResponse.statusCode,
          content: jsonDecode(httpResponse.body));
    } on Exception catch (e) {
      response = Response(statusCode: 500, exception: e);
    }

    return response;
  }
}
