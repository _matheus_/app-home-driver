import 'package:app_driver/data/models/pendecy_model.dart';
import 'package:app_driver/data/repositories/pendency_repository_interdace.dart';

import 'package:app_driver/data/repositories/user_repository_interface.dart';
import 'package:app_driver/data/services/http_service.dart';
import 'package:app_driver/shared/constants.dart';

class PendencyRepository implements PendencyRepositoryInterface {
  final HttpService httpService;
  final UserRepositoryInterface userRepository;

  PendencyRepository({required this.httpService, required this.userRepository});

  @override
  Future<PendencyModel> getPendencies(String driverId) async {
    final uri = '${Constants.baseUrlApiDriver}/driver-pendency';
    final headers = {
      'Authorization': 'Bearer ${await userRepository.getToken()}'
    };
    final response = await httpService.getRequest(uri,
        params: {'driver_id': driverId}, headers: headers);

    if (response.success) {
      return PendencyModel.fromJson(response.content!);
    }

    throw Exception('Falha ao carregar pendências');
  }
}
