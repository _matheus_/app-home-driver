import 'package:app_driver/data/models/user_model.dart';
import 'package:app_driver/data/repositories/user_repository_interface.dart';
import 'package:app_driver/data/services/http_service.dart';
import 'package:app_driver/shared/constants.dart';

class UserRepository implements UserRepositoryInterface {
  final HttpService httpService;
  UserModel? _user;

  UserRepository({required this.httpService});

  @override
  Future<UserModel> getUser() async {
    if (_user != null) {
      return _user!;
    }

    final uri = '${Constants.baseUrlApiDriver}/users-me';

    final response = await httpService.getRequest(uri,
        headers: {'Authorization': 'Bearer ${await getToken()}'});

    if (response.success) {
      _user = UserModel.fromJson(response.content!['data']);
      return _user!;
    }

    throw Exception('Falha ao obter dados do usuário');
  }

  @override
  Future<String> getToken() {
    const token =
        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL3d3dy5tZXJ5dG8uY29tLmJyLyIsImlhdCI6MTYzNTk1MDEwOSwibmJmIjoxNjM1OTUwMTA5LCJleHAiOjE2MzY1NTQ5MDksImRhdGEiOnsidXNlciI6eyJpZCI6IjUwMCIsInNlc3Npb24iOiJiMGFjNDkxYzgyNDA0ZDQ0OTdmYWQ0ODY4MDliMjQ1YiJ9fX0.Bg-m4xfIrm8SpQmegiNMPsESrawE6r8gSWA010Vp1t4';

    return Future.value(token);
  }
}
