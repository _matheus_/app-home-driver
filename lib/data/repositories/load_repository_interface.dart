import 'package:app_driver/data/models/load_model.dart';

abstract class LoadRepositoryInterface {
  Future<LoadModel> getLoads(String driverId);
}
