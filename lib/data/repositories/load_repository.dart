import 'package:app_driver/data/models/load_model.dart';
import 'package:app_driver/data/repositories/load_repository_interface.dart';
import 'package:app_driver/data/services/http_service.dart';
import 'package:app_driver/shared/constants.dart';

class LoadRepository implements LoadRepositoryInterface {
  final HttpService httpService;

  LoadRepository({required this.httpService});

  @override
  Future<LoadModel> getLoads(String driverId) async {
    final uri = '${Constants.baseUrlApiDriver}/load-truck';
    final response =
        await httpService.getRequest(uri, params: {'driver_id': driverId});

    if (response.success) {
      return LoadModel.fromJson(response.content!);
    }

    return throw Exception('Falha ao obter carregamentos');
  }
}
