import 'package:app_driver/data/models/pendecy_model.dart';

abstract class PendencyRepositoryInterface {
  Future<PendencyModel> getPendencies(String driverId);
}
