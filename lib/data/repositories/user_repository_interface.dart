import 'package:app_driver/data/models/user_model.dart';

abstract class UserRepositoryInterface {
  Future<UserModel> getUser();

  Future<String> getToken();
}
