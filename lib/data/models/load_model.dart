class LoadModel {
  List<LoadItem> items;
  int total;

  LoadModel({
    required this.items,
    required this.total,
  });

  factory LoadModel.fromJson(Map<String, dynamic> json) {
    return LoadModel(
        items:
            List<LoadItem>.from(json['items'].map((j) => LoadItem.fromJson(j))),
        total: json['total']);
  }

  bool get hasLoad => total > 0;
}

class LoadItem {
  int id;
  bool accepted;
  int dispatchedBoxes;

  LoadItem({
    required this.id,
    required this.accepted,
    required this.dispatchedBoxes,
  });

  factory LoadItem.fromJson(Map<String, dynamic> json) {
    return LoadItem(
        id: json['id'],
        accepted: json['accepted'],
        dispatchedBoxes: json['dispatched_boxes'] ?? 0);
  }
}
