class PendencyModel {
  int boxes;
  List<int> orders;

  PendencyModel({
    required this.boxes,
    required this.orders,
  });

  factory PendencyModel.fromJson(Map<String, dynamic> json) {
    return PendencyModel(
        boxes: json['boxes'],
        orders: List.castFrom<dynamic, int>(json['orders']));
  }

  bool get hasPendency => orders.isNotEmpty && boxes > 0;
}
