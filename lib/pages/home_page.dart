import 'package:app_driver/blocs/home_bloc.dart';
import 'package:app_driver/data/repositories/load_repository.dart';
import 'package:app_driver/data/repositories/load_repository_interface.dart';
import 'package:app_driver/data/repositories/pendency_repository.dart';
import 'package:app_driver/data/repositories/pendency_repository_interdace.dart';
import 'package:app_driver/data/repositories/user_repository.dart';
import 'package:app_driver/data/repositories/user_repository_interface.dart';
import 'package:app_driver/data/services/http_service.dart';
import 'package:app_driver/shared/constants.dart';
import 'package:app_driver/widgets/custom_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final httpService = HttpService();
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<UserRepositoryInterface>(
          create: (_) => UserRepository(httpService: httpService),
        ),
        RepositoryProvider<LoadRepositoryInterface>(
          create: (_) => LoadRepository(httpService: httpService),
        ),
        RepositoryProvider<PendencyRepositoryInterface>(
          create: (context) => PendencyRepository(
              httpService: httpService,
              userRepository: context.read<UserRepositoryInterface>()),
        )
      ],
      child: BlocProvider<HomeCubit>(
        create: (blocContext) => HomeCubit(
          loadRepository: blocContext.read<LoadRepositoryInterface>(),
          pendencyRepository: blocContext.read<PendencyRepositoryInterface>(),
          userRepository: blocContext.read<UserRepositoryInterface>(),
        )..loadHome(),
        child: const HomeView(),
      ),
    );
  }
}

class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const Icon(Icons.menu),
        title: const Text('Facily Driver'),
        actions: [
          IconButton(
            onPressed: () {
              context.read<HomeCubit>().loadHome();
            },
            icon: const Icon(Icons.refresh),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(14.0),
          child: BlocBuilder<HomeCubit, HomeState>(
            builder: (context, state) {
              if (state is LoadingState) {
                return const Center(child: CircularProgressIndicator());
              } else if (state is HomeLoaded) {
                return _buildHomeLoaded(state);
              }

              return const Center(child: Text('Falha ao carregar informações'));
            },
          ),
        ),
      ),
    );
  }

  Widget _buildHomeLoaded(HomeLoaded state) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Olá, ${state.user.firstName}, ${state.user.lastName}',
          style: const TextStyle(fontWeight: FontWeight.w800),
        ),
        const Text('Tudo bem?'),
        const SizedBox(height: 5.0),
        Visibility(
          visible: !state.pendency.hasPendency,
          child: _buildLoadAlert(),
        ),
        const SizedBox(height: 20.0),
        Visibility(
          visible: state.load.total > 0,
          child: _buildMainOptions(),
        ),
        const SizedBox(height: 20.0),
        _buildIdCard(state.user.id)
      ],
    );
  }

  Widget _buildLoadAlert() {
    return Container(
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
          color: Colors.green[200],
          borderRadius: const BorderRadius.all(Radius.circular(5.0))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: const [
          Text(
            'Carregamento liberado',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          Text('Dirija-se ao setor responsável e faça um novo carregamento')
        ],
      ),
    );
  }

  Widget _buildMainOptions() {
    return Container(
      padding: const EdgeInsets.all(12.0),
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.grey[200],
          borderRadius: const BorderRadius.all(Radius.circular(5.0))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: const [
          Text(
            'O que você deseja fazer?',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          Text(
            'Selecione a opção abaixo',
            style: TextStyle(fontSize: 12.0),
          ),
          SizedBox(height: 10.0),
          CustomButton(label: 'Verificar carregamentos'),
          SizedBox(height: 15.0),
          CustomButton(label: 'Entregar pedidos'),
          SizedBox(height: 15.0),
          CustomButton(label: 'Consultar NFe')
        ],
      ),
    );
  }

  Widget _buildIdCard(String userId) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.grey[200],
          borderRadius: const BorderRadius.all(Radius.circular(5.0))),
      child: Row(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text('N de identificação:'),
                  Text(
                    userId,
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 15.0),
                  const Text(
                    'Utilize o QRCode sempre que for necessário continuar sua identifição',
                    style: TextStyle(fontSize: 12.0),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Image.network(Constants.qrCodeImageGenerator + userId),
            ),
          ),
        ],
      ),
    );
  }
}
