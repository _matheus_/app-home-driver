import 'package:app_driver/data/repositories/load_repository_interface.dart';
import 'package:app_driver/data/repositories/pendency_repository_interdace.dart';
import 'package:app_driver/data/repositories/user_repository_interface.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:app_driver/data/models/load_model.dart';
import 'package:app_driver/data/models/pendecy_model.dart';
import 'package:app_driver/data/models/user_model.dart';

abstract class HomeState {}

class InitialState extends HomeState {}

class LoadingState extends HomeState {}

class FailedLoadingHome extends HomeState {
  final Exception error;

  FailedLoadingHome(this.error);
}

class HomeLoaded extends HomeState {
  UserModel user;
  LoadModel load;
  PendencyModel pendency;

  HomeLoaded({
    required this.user,
    required this.load,
    required this.pendency,
  });
}

class HomeCubit extends Cubit<HomeState> {
  final UserRepositoryInterface userRepository;
  final LoadRepositoryInterface loadRepository;
  final PendencyRepositoryInterface pendencyRepository;

  HomeCubit(
      {required this.userRepository,
      required this.loadRepository,
      required this.pendencyRepository})
      : super(InitialState());

  Future<void> loadHome() async {
    emit(LoadingState());

    try {
      final user = await userRepository.getUser();
      final load = await loadRepository.getLoads(user.id);
      final pendency = await pendencyRepository.getPendencies(user.id);

      emit(HomeLoaded(user: user, load: load, pendency: pendency));
    } on Exception catch (e) {
      emit(FailedLoadingHome(e));
    }
  }

  Future<void> emitFail() async {
    emit(FailedLoadingHome(Exception('')));
    await Future.delayed(const Duration(seconds: 1));
    emit(InitialState());
  }
}
