abstract class Constants {
  static const baseUrlApiDriver =
      'https://hmlg-logistics-mobile-driver-cnbi5ucx.ue.gateway.dev/api/v1';

  static const qrCodeImageGenerator =
      'https://southamerica-east1-facily-817c2.cloudfunctions.net/generate_qr_code?data=';
}
